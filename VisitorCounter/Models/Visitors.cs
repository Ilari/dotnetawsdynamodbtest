using System;

namespace VisitorCounter.Models
{
    public class Visitors
    {
        public int Id { get; set; }

        public int VisitorCount { get; set; }
    }
}
