﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamoDb.Lib.DynamoDb;
using Microsoft.AspNetCore.Mvc;

namespace VisitorCounter.Controllers
{
    [Produces("application/json")]
    [Route("api/DynamoDb")]
    public class VisitorController : Controller
    {
        private readonly IDynamoDbTest _dynamoDb;

        public VisitorController(IDynamoDbTest dynamoDb)
        {
            _dynamoDb = dynamoDb;
        }

        [Route("update")]
        public IActionResult Index()
        {
            _dynamoDb.CreateTable();
            
            return Ok();
        }
    }
}