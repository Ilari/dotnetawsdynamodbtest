﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;

namespace DynamoDb.Lib.DynamoDb
{
    public class DynamoDbTest : IDynamoDbTest
    {

        private readonly IAmazonDynamoDB _dynamoDbClient;
        private static readonly string tableName = "TestTable";

        public DynamoDbTest(IAmazonDynamoDB dynamoDbClient)
        {
            _dynamoDbClient = dynamoDbClient;
        }


        public void CreateTable()
        {
            try
            {
                CreateTempTable();
            } 
            catch
            {

            }

        }

        private void CreateTempTable()
        {
            Console.WriteLine("Creating Table");

            var request = new CreateTableRequest
            {
                AttributeDefinitions = new List<AttributeDefinition>
                {
                    new AttributeDefinition
                    {
                        AttributeName = "Id",
                        AttributeType = "N"
                    },
                    new AttributeDefinition
                    {
                        AttributeName = "ReplyDateTime",
                        AttributeType = "N"
                    }

                },
                KeySchema = new List<KeySchemaElement>
                {
                    new KeySchemaElement
                    {
                        AttributeName = "Id",
                        KeyType = "HASH"
                    },
                    new KeySchemaElement
                    {
                        AttributeName = "ReplyDateTime",
                        KeyType = "Range"
                    },
                },
                ProvisionedThroughput = new ProvisionedThroughput
                {
                    ReadCapacityUnits = 5,
                    WriteCapacityUnits = 5
                },
                TableName = tableName
            };

            var response = _dynamoDbClient.CreateTableAsync(request);

            WaitUntilTableReady(tableName);
        }

        private void WaitUntilTableReady(string tableName)
        {
            string status = null;

            do
            {
                Thread.Sleep(5000);

                try
                {
                    var res = _dynamoDbClient.DescribeTableAsync(new DescribeTableRequest {
                        TableName = tableName
                    });

                    status = res.Result.Table.TableStatus;

                }
                catch (ResourceNotFoundException)
                {

                }
                
            } 
            while (status != "ACTIVE");
            {
              
                Console.WriteLine("Table Created");
            }
        }
    }
}
